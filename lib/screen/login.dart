import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopping_cart/screen/signin.dart';
import 'package:shopping_cart/utils/asset_path.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          decoration: _background(),
          child: Align(
            alignment: Alignment.center,
            child: Column(
              children: [
                _logo(),
                _text(),
                _text1(),
                _getting_started(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container _getting_started() {
    return Container(
      width: 400,
      height: 60,
      child: ElevatedButton(
          onPressed: (() {
            Get.to(SigninScreen());
          }),
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Color(0xff53B175)),
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))))),
          child: Text(
            'Getting Started',
            style: TextStyle(
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.w600,
                fontSize: 16,
                color: Colors.white),
          )),
    );
  }

  Image _text1() {
    return Image(
      image: AssetImage(AssetPath.text1),
      height: 50,
    );
  }

  Image _text() => Image(image: AssetImage(AssetPath.text));

  Padding _logo() {
    return Padding(
      padding: const EdgeInsets.only(
        top: 370,
        bottom: 50,
      ),
      child: Image(
        image: AssetImage(AssetPath.logo),
      ),
    );
  }

  BoxDecoration _background() {
    return BoxDecoration(
      image: DecorationImage(
          image: AssetImage(AssetPath.background), fit: BoxFit.cover),
    );
  }
}
