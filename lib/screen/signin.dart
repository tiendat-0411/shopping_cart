import 'package:flutter/material.dart';
import 'package:shopping_cart/utils/asset_path.dart';
import 'package:shopping_cart/widget/login.dart';
import 'package:shopping_cart/widget/signup.dart';

class SigninScreen extends StatefulWidget {
  const SigninScreen({super.key});

  @override
  State<SigninScreen> createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AssetPath.background1), fit: BoxFit.fill)),
          child: Column(
            children: [
              _carrot(),
              _textlog(),
              _email(),
              _input_email(),
              _password(),
              _input_password(),
              _forgot_password(),
              LogIn(),
              SignUp()
            ],
          ),
        ),
      ),
    );
  }

  Padding _forgot_password() {
    return Padding(
      padding: EdgeInsets.only(top: 20, right: 30),
      child: Align(
        alignment: Alignment.bottomRight,
        child: Image(
          image: AssetImage(AssetPath.forgotpassword),
        ),
      ),
    );
  }

  Padding _input_password() {
    return Padding(
      padding: EdgeInsets.only(left: 44, right: 40),
      child: TextField(
        decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xffE2E2E2),
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xffE2E2E2),
              ),
            )),
      ),
    );
  }

  Padding _password() {
    return Padding(
      padding: const EdgeInsets.only(top: 50, left: 44),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Image(image: AssetImage(AssetPath.password)),
      ),
    );
  }

  Padding _input_email() {
    return Padding(
      padding: EdgeInsets.only(left: 44, right: 40),
      child: TextField(
        decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xffE2E2E2),
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xffE2E2E2),
              ),
            )),
      ),
    );
  }

  Padding _email() {
    return Padding(
      padding: const EdgeInsets.only(top: 50, left: 44),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Image(
          image: AssetImage(AssetPath.email),
        ),
      ),
    );
  }

  Padding _textlog() {
    return Padding(
      padding: EdgeInsets.only(top: 70.21, left: 30, right: 184.82),
      child: Image(image: AssetImage(AssetPath.textlog)),
    );
  }

  Padding _carrot() {
    return Padding(
      padding: const EdgeInsets.only(top: 77.25),
      child: Image(image: AssetImage(AssetPath.carrot)),
    );
  }
}
