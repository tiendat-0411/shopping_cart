import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:shopping_cart/screen/SignUp.dart';


class SignUp extends StatelessWidget {
  const SignUp({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
                padding: const EdgeInsets.only( top: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: RichText(
                    text: TextSpan(
                        text: "Don't have account ?",
                        style: const TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                            fontFamily: 'Gilroy'),
                        children: [
                          TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = (() {
                               Get.to(SignUpScreen());
                              }),
                            text: " Signup",
                            style: const TextStyle(
                                fontSize: 16,
                                fontFamily: 'Gilroy',
                                color: Colors.blue,
                                fontWeight: FontWeight.w600
                                ),
                          )
                        ]),
                  ),
                ),
              );
  }
}