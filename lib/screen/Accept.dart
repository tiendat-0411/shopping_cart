import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:shopping_cart/utils/asset_path.dart';

class OrderAcceptScreen extends StatelessWidget {
  const OrderAcceptScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return 
    Scaffold(
        body: Container(
          width: double.infinity,
          decoration: _layer(),
          child: Column(
            children: [
              _accept_image(),
              _text_accepted(),
              _text_placed(),
              _track_oder(),
              _back_to_home()
            ],
          ),
        ),
      );
    
  }

  Padding _back_to_home() {
    return Padding(
      padding:  EdgeInsets.only(top: 30),
      child: RichText(
        text: TextSpan(
          recognizer: TapGestureRecognizer()
            ..onTap = (() {
              Get.to(BottomAppBar());
            }),
          text: 'Back to home',
          style: TextStyle(
              fontSize: 14, color: Colors.black, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }

  Padding _track_oder() {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Container(
        width: 380,
        height: 60,
        child: ElevatedButton(
            onPressed: (() {
              Get.to(OrderAcceptScreen());
            }),
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Color(0xff53B175)),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(18))))),
            child: Text(
              'Track Order',
              style: TextStyle(
                  fontFamily: 'Gilroy',
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            )),
      ),
    );
  }

  Padding _text_placed() {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: Text(
        'Your items has been placcd and is on \n          it’s way to being processed',
        style: TextStyle(fontSize: 16, color: Color(0xff7C7C7C)),
      ),
    );
  }

  Padding _text_accepted() {
    return Padding(
      padding: const EdgeInsets.only(top: 50, left: 50, right: 40),
      child: Text(
        'Your Order has been \n          accepted ',
        style: TextStyle(fontSize: 28, fontWeight: FontWeight.w400),
      ),
    );
  }

  Container _accept_image() {
    return Container(
      padding: EdgeInsets.only(top: 170, right: 40),
      child: Image(image: AssetImage(AssetPath.accept)),
    );
  }

  BoxDecoration _layer() {
    return BoxDecoration(
        image: DecorationImage(
            image: AssetImage(
              AssetPath.layer,
            ),
            fit: BoxFit.cover));
  }
}
