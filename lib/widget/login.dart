
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'package:shopping_cart/widget/bottom_appbar.dart';

class LogIn extends StatelessWidget {
  const LogIn({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
                height: 70,
                width: 380,
                child: Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color(0xff53B175),
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                    ),
                    child: ElevatedButton(
                        onPressed: (() {
                          Get.to(bottom_appbar());
                        }),
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(Color(0xff53B175)),
                         shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15))))            
                        ),
                        child: Text('Log In',
                        style: TextStyle(
                          fontFamily: 'Gilroy',
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w600
                        ),)
                     )
                  ),
                ),
              );
  }
}