import 'package:flutter/material.dart';

import 'package:shopping_cart/utils/asset_path.dart';
import 'package:shopping_cart/widget/list_products1.dart';
import 'package:shopping_cart/widget/list_products.dart';
import 'package:shopping_cart/widget/list_products2.dart';
import 'package:shopping_cart/widget/search_store.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({super.key});
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Carrot(),
            Searchstore(),
            Banner(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _offer_text(),
                _see_all_text(),
              ],
            ),
            ListFruit(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _bestselling_text(),
                _see_all_text(),
              ],
            ),
            ListChoice1(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _groceries_text(),
                _see_all_text(),
              ],
            ),
            _row_image(),
            ListMeat()
          ],
        ),
      ),
    );
  }
}

class _row_image extends StatelessWidget {
  const _row_image({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Image(image: AssetImage(AssetPath.pulses)),
      Image(image: AssetImage(AssetPath.rice)),
    ]);
  }
}

class _groceries_text extends StatelessWidget {
  const _groceries_text({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(top: 10, left: 15),
        child: Container(
          child: Text(
            'Groceries',
            style: TextStyle(
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.w600,
                fontSize: 18),
          ),
        ),
      ),
    );
  }
}

class _bestselling_text extends StatelessWidget {
  const _bestselling_text({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 15, top: 10),
        child: Container(
          child: Text(
            'Best Selling',
            style: TextStyle(
                fontFamily: 'Gilroy',
                fontWeight: FontWeight.w600,
                fontSize: 18),
          ),
        ),
      ),
    );
  }
}

class _see_all_text extends StatelessWidget {
  const _see_all_text({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: EdgeInsets.only(top: 10, right: 15),
        child: Text(
          'See all',
          style: TextStyle(
              fontFamily: 'Giroy',
              fontWeight: FontWeight.w600,
              fontSize: 16,
              color: Color(0xff53B175)),
        ),
      ),
    );
  }
}

class _offer_text extends StatelessWidget {
  const _offer_text({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(top: 10, left: 15),
        child: Container(
          child: Text(
            'Exclusive Offer',
            style: TextStyle(
                fontFamily: 'Gordita_Bold',
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
        ),
      ),
    );
  }
}

class Banner extends StatelessWidget {
  const Banner({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 15, right: 15),
      child: Container(
        child: Image(
          image: AssetImage(AssetPath.banner),
          width: 700,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class Carrot extends StatelessWidget {
  const Carrot({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Align(
        alignment: Alignment.topCenter,
        child: Image(
          image: AssetImage(AssetPath.carrot),
        ),
      ),
    );
  }
}
