import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shopping_cart/screen/Login.dart';
import 'package:shopping_cart/utils/asset_path.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Timer(
      Duration(seconds: 1),
      (() => Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: ((context) => LoginScreen()),
            ),
          )),
    );
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xff53B175),
        body: Center(
          child: Container(
            child: Image(image: AssetImage(AssetPath.logo_splash)),
          ),
        ),
      ),
    );
  }
}
