import 'package:flutter/material.dart';

import 'package:shopping_cart/widget/list_egg.dart';
import 'package:shopping_cart/widget/search_store.dart';

class EggScreen extends StatefulWidget {

  const EggScreen({super.key, });
  @override
  State<EggScreen> createState() => _EggScreenState();
}

class _EggScreenState extends State<EggScreen> {
  @override
  Widget build(BuildContext context, ) {
    return MaterialApp(
        home: Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [Searchstore(), ListEgg()],
        ),
      ),
    ));
  }
}
