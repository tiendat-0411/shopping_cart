import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'package:shopping_cart/controller/fruits_controller.dart';
import 'package:shopping_cart/models/Choice.dart';

import 'package:shopping_cart/utils/asset_path.dart';

class DrinksProducts extends StatelessWidget {
  final CardController controller = Get.find();

  DrinksProducts({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Material(
        child: SizedBox(
          height: 200,
          child: ListView.builder(
            itemCount: controller.products.length,
            itemBuilder: (BuildContext context, int index) {
              return DrinkProducts(
                controller: controller,
                product: controller.products.keys.toList()[index],
                quantity: controller.products.values.toList()[index],
                index: index,
              );
            },
          ),
        ),
      ),
    );
  }
}

class DrinkProducts extends StatelessWidget {
  final CardController controller;
  final Product product;
  final int quantity;
  final int index;

  const DrinkProducts(
      {Key? key,
      required this.controller,
      required this.product,
      required this.quantity,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Container(
              height: 73,
              width: 90,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                products3[index].image.toString(),
                  ),
                ),
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: RichText(
                      text: TextSpan(
                          text: '${products3[index].name.toString()}',
                          style: TextStyle(
                              fontFamily: 'Gilroy-bold',
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                              fontSize: 16)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 100, right: 10),
                    child: IconButton(
                        onPressed: () {
                          controller.removeProduct(product);
                        },
                        icon: SvgPicture.asset(AssetPath.x)),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(right: 220),
                child: RichText(
                  text: TextSpan(
                      text: '${products3[index].unit.toString()}\n',
                      style: TextStyle(fontSize: 14, color: Color(0xff7C7C7C))),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () {
                      controller.removeProduct(product);
                    },
                    icon: Icon(Icons.remove_circle),
                  ),
                  Text('$quantity'),
                  IconButton(
                    onPressed: () {
                      controller.addProduct(product);
                    },
                    icon: Icon(Icons.add_circle),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: RichText(
                        text: TextSpan(
                            text: r" $"
                                '${products3[index].price.toString()}\n',
                            style: TextStyle(
                                fontFamily: 'Gilroy-bold',
                                fontWeight: FontWeight.w600,
                                fontSize: 17,
                                color: Colors.black))),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
