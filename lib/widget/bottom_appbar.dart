import 'package:flutter/material.dart';
import 'package:shopping_cart/screen/Accept.dart';
import 'package:shopping_cart/screen/Account.dart';
import 'package:shopping_cart/screen/Cart.dart';
import 'package:shopping_cart/screen/Favourite.dart';
import 'package:shopping_cart/screen/Find.dart';
import 'package:shopping_cart/screen/Home.dart';

class bottom_appbar extends StatefulWidget {
  const bottom_appbar({super.key});

  @override
  State<bottom_appbar> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<bottom_appbar> {
  int _selectedIndex = 0;
  final List screen = [
    HomeScreen(),
    FindScreen(),
    CartScreen(),
    FavouriteScreen(),
    ACcountSCreen(),
    OrderAcceptScreen()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: screen[_selectedIndex],
      ),
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20),
          topLeft: Radius.circular(20),
        ),
        child: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list_alt),
              label: 'Explore',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart),
              label: 'Cart',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite_outline_outlined),
              label: 'Favourite',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_box_rounded),
              label: 'Account',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.green,
          unselectedItemColor: Colors.black,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
