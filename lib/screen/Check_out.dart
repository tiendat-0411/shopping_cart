import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'package:shopping_cart/controller/fruits_controller.dart';
import 'package:shopping_cart/screen/Accept.dart';
import 'package:shopping_cart/screen/SignUp.dart';

import 'package:shopping_cart/utils/asset_path.dart';

class CheckOutScreen extends StatelessWidget {
  final d = Get.put(CardController());
  CheckOutScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Check_Out();
  }
}

class Check_Out extends StatelessWidget {
  // final DrinkController controller = Get.find();
  Check_Out({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      SafeArea(
        bottom: true,
        child: Container(
          width: 400,
          height: 60,
          child: ElevatedButton(
              onPressed: (() {
                showModalBottomSheet(
                  context: context,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  builder: (context) => Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 6, top: 20),
                            child: Text(
                              'Checkout',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 24),
                            ),
                          ),
                          IconButton(
                              onPressed: (() {
                                Get.back();
                              }),
                              icon: SvgPicture.asset(AssetPath.x)),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8, right: 8, bottom: 20),
                        child: Container(
                          height: 1,
                          color: Color(0xffE2E2E2B2),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              'Delivery',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                Get.to(CheckOutScreen());
                              },
                              icon: SvgPicture.asset(AssetPath.next))
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8, right: 8, bottom: 20),
                        child: Container(
                          height: 1,
                          color: Color(0xffE2E2E2B2),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Pament',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                Get.to(CheckOutScreen());
                              },
                              icon: SvgPicture.asset(AssetPath.next))
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8, right: 8, bottom: 20),
                        child: Container(
                          height: 1,
                          color: Color(0xffE2E2E2B2),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Promo Code',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                Get.to(CheckOutScreen());
                              },
                              icon: SvgPicture.asset(AssetPath.next))
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8, right: 8, bottom: 20),
                        child: Container(
                          height: 1,
                          color: Color(0xffE2E2E2B2),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Total Cost',
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          IconButton(
                              onPressed: () {
                                Get.to(CheckOutScreen());
                              },
                              icon: SvgPicture.asset(AssetPath.next))
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8, right: 8, bottom: 20),
                        child: Container(
                          height: 1,
                          color: Color(0xffE2E2E2B2),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(),
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: RichText(
                            text: TextSpan(
                                text: "By placing an order you agree to our ",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.black,
                                    fontFamily: 'Gilroy-Medium'),
                                children: [
                                  TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = (() {
                                        Get.to(SignUpScreen());
                                      }),
                                    text: "Terms",
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: 'Gilroy',
                                      color: Colors.blue,
                                    ),
                                  ),
                                  TextSpan(
                                    text: ' and',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: 'Gilroy-bold',
                                        color: Colors.black),
                                  ),
                                  TextSpan(
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = (() {
                                          Get.to(SignUpScreen());
                                        }),
                                      text: " Conditions",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: 'Gilroy-bold',
                                        color: Colors.blue,
                                      ))
                                ]),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Container(
                          width: 380,
                          height: 60,
                          child: ElevatedButton(
                              onPressed: (() {
                                Get.to(OrderAcceptScreen());
                              }),
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      Color(0xff53B175)),
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(18))))),
                              child: Text(
                                'Place Order',
                                style: TextStyle(
                                    fontFamily: 'Gilroy',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              )),
                        ),
                      ),
                    ],
                  ),
                );
              }),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Color(0xff53B175)),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15))))),
              child: Text(
                'Go to checkout',
                style: TextStyle(
                    fontFamily: 'Gilroy',
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              )),
        ),
      ),
    ]);
  }
}
