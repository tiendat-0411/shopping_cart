import 'package:flutter/material.dart';

import 'package:shopping_cart/widget/list_find.dart';
import 'package:shopping_cart/widget/search_store.dart';

class FindScreen extends StatefulWidget {
  const FindScreen({super.key,});

  @override
  State<FindScreen> createState() => _FindScreenState();
}

class _FindScreenState extends State<FindScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 70,
          ),
          FindProducts(),
          Searchstore(),
          ListFind(),
        ],
      ),
    );
  }
}

class FindProducts extends StatelessWidget {
  const FindProducts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('Find Products',
    style: TextStyle(
      color: Colors.black,
      fontSize: 25,
      fontWeight: FontWeight.bold,
      
    ),);
  }
}


