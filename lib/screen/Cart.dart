import 'package:flutter/material.dart';
import 'package:shopping_cart/widget/cart_prodcuts.dart';
import 'package:shopping_cart/screen/Check_out.dart';

class CartScreen extends StatelessWidget {
  
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          SizedBox(
            height: 70,
          ),
          Cart(),
          CartProducts(),     
          CheckOutScreen()
        ],
      ),
    ));
  }
}

class Cart extends StatelessWidget {
  const Cart({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Cart',
        style: TextStyle(
            color: Colors.black, fontSize: 25, fontWeight: FontWeight.bold),
      ),
    );
  }
}
