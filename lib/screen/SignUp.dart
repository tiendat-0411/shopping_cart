import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopping_cart/screen/SignIn.dart';
import 'package:shopping_cart/utils/asset_path.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({super.key});

  @override
  State<SignUpScreen> createState() => _SignUpState();
}

class _SignUpState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          decoration: _background1(),
          child: Column(
            children: [
              _carrot(),
              _signup(),
              _username(),
              _input_username(),
              _email(),
              _input_email(),
              _password(),
              _input_password(),
              _continue_text(context),
              _sign_up(),
              _sign_in(),
            ],
          ),
        ),
      ),
    );
  }

  Padding _sign_in() {
    return Padding(
              padding: const EdgeInsets.only(top: 25),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RichText(
                  text: TextSpan(
                    text: "Already have an account ?",
                    style: const TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Gilroy-Medium'),
                    children: [
                      TextSpan(
                        recognizer: TapGestureRecognizer()
                          ..onTap = (() {
                           Get.to(SigninScreen());
                          }),
                        text: " Signin",
                        style: const TextStyle(
                            fontSize: 15,
                            fontFamily: 'Gilroy',
                            color: Colors.blue,
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
              ),
            );
  }

  Padding _sign_up() {
    return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Container(
              height: 60,
                width: 380,
                child: ElevatedButton(
                    onPressed: (() {
                      Get.to(SignUpScreen());
                    }),
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Color(0xff53B175)),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15))))),
                    child: Text(
                      'Sign Up',
                      style: TextStyle(
                          fontFamily: 'Gilroy',
                          fontWeight: FontWeight.w600,
                          fontSize: 18,
                          color: Colors.white),
                    )),
              ),
            );
  }

  Padding _continue_text(BuildContext context) {
    return Padding(
              padding: const EdgeInsets.only(top: 25, left: 33, right: 30),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RichText(
                  text: TextSpan(
                      text: "By continuting you agree to our ",
                      style: const TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontFamily: 'Gilroy-Medium'),
                      children: [
                        TextSpan(
                          recognizer: TapGestureRecognizer()
                            ..onTap = (() {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const SignUpScreen(),
                                ),
                              );
                            }),
                          text: " Terms of Service",
                          style: const TextStyle(
                            fontSize: 15,
                            fontFamily: 'Gilroy',
                            color: Colors.blue,
                          ),
                        ),
                        TextSpan(
                          text: ' and',
                          style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Gilroy-bold',
                              color: Colors.black),
                        ),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = (() {
                               Get.to(SignUpScreen());
                              }),
                            text: " Privacy Policy.",
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Gilroy-bold',
                              color: Colors.blue,
                            ))
                      ]),
                ),
              ),
            );
  }

  Padding _input_password() {
    return Padding(
            padding: EdgeInsets.only(left: 34, right: 40),
            child: TextField(
              decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffE2E2E2),
                    ),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffE2E2E2),
                    ),
                  )),
            ),
          );
  }

  Padding _input_email() {
    return Padding(
            padding: EdgeInsets.only(left: 34, right: 40),
            child: TextField(
              decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffE2E2E2),
                    ),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffE2E2E2),
                    ),
                  )),
            ),
          );
  }


  Padding _input_username() {
    return Padding(
            padding: EdgeInsets.only(left: 34, right: 40),
            child: TextField(
              decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffE2E2E2),
                    ),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xffE2E2E2),
                    ),
                  )),
            ),
          );
  }

  Padding _password() {
    return Padding(
              padding: const EdgeInsets.only(top: 30, left: 34),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Image(image: AssetImage(AssetPath.password)),
              ),
            );
  }


  Padding _email() {
    return Padding(
              padding: const EdgeInsets.only(top: 30, left: 34),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Image(
                  image: AssetImage(AssetPath.email),
                ),
              ),
            );
  }


  Padding _username() {
    return Padding(
              padding: const EdgeInsets.only(top: 33, left: 31),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Image(
                  image: AssetImage(AssetPath.Username),
                ),
              ),
            );
  }

  Padding _signup() {
    return Padding(
              padding: EdgeInsets.only(top: 50.21, right: 150.82),
              child: Image(image: AssetImage(AssetPath.signup)),
            );
  }

  Padding _carrot() {
    return Padding(
              padding: const EdgeInsets.only(
                top: 77.25,
              ),
              child: Image(image: AssetImage(AssetPath.carrot)),
            );
  }

  BoxDecoration _background1() {
    return BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AssetPath.background1), fit: BoxFit.fill),
        );
  }
}
