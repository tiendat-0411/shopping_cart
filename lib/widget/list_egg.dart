
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shopping_cart/models/Choice.dart';
import 'package:shopping_cart/utils/asset_path.dart';

class ListEgg extends StatelessWidget {
 
  const ListEgg({super.key, });
  @override
  Widget build(BuildContext context) {
    return  Container(
              height: 650,
              child: GridView.count(          
                crossAxisCount: 2,
                children: List.generate(products4.length, (index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        left: 15, right: 15, bottom: 15, top: 15),
                    child: Container(                   
                      decoration: BoxDecoration(
                      color: Color(0xff53B175B2),
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      border: Border.all(
                        width: 1.0,
                        color: Color(0xffF8A44CB2),
                      ),
                    ),
                      child: Column(
                        children: [                        
                          Container(
                          height: 50,                          
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                  products4[index].image.toString(),
                                ),
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30))),
                        ),
                          RichText(
                          maxLines: 2,
                          text: TextSpan(
                              text: '${products4[index].name.toString()}\n',
                              style: TextStyle(
                                  fontFamily: 'Gilroy-bold',
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black,
                                  fontSize: 18)),
                        ),
                          Align(
                          alignment: Alignment.centerLeft,
                          child: RichText(
                            text: TextSpan(
                                text:
                                    '${products4[index].unit.toString()}\n',
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Color(0xff7C7C7C))),
                          ),
                        ),
                           Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 5),
                                child: RichText(
                                    text: TextSpan(
                                        text: r" $"
                                            '${products4[index].price.toString()}\n',
                                        style: TextStyle(
                                            fontFamily: 'Gilroy-bold',
                                            fontWeight: FontWeight.w600,
                                            fontSize: 17,
                                            color: Colors.black))),
                              ),
                            ),
                              Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50))),
                            child: IconButton(
                                onPressed: () {
                                //  Get.to(drinkscreen());
                                },
                                icon: SvgPicture.asset(AssetPath.plus)),
                          ),
                        ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              ),
            );
  }
}