import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'package:shopping_cart/controller/fruits_controller.dart';
import 'package:shopping_cart/models/Choice.dart';
import 'package:shopping_cart/utils/asset_path.dart';

class CartProducts extends StatelessWidget {
 
  final CardController controller = Get.find();

  CartProducts({Key? key,}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Material(
        child: SizedBox(
          height: 600,
          child: ListView.builder(
            itemCount: controller.products.length,
            itemBuilder: (BuildContext context, int index) {
              return CardProductCard(
                controller: controller,
                product: controller.products.keys.toList()[index],
                quantity: controller.products.values.toList()[index],
                index: index,
              );
            },
          ),
        ),
      ),
    );
  }
}

class CardProductCard extends StatelessWidget {
  final CardController controller;
  final Product product;
  final int quantity;
  final int index;

  const CardProductCard(
      {Key? key,
      required this.controller,
      required this.product,
      required this.quantity,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
   mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(25),
          child: Container(
            height: 73,
            width: 100,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  products[index].image.toString(),
                ),
              ),
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              text: TextSpan(
                  text: '${products[index].name.toString()}',
                  style: TextStyle(
                      fontFamily: 'Gilroy-bold',
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      fontSize: 18)),
            ),
        
            Padding(
              padding:  EdgeInsets.only(top: 8),
              child: RichText(
                text: TextSpan(
                    text: '${products[index].unit.toString()}\n',
                    style: TextStyle(fontSize: 16, color: Color(0xff7C7C7C))),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () {
                    controller.removeProduct(product);
                  },
                  icon: const Icon(Icons.remove_circle),
                ),
                Text('$quantity'),
                IconButton(
                  onPressed: () {
                    controller.addProduct(product);
                  },
                  icon: const Icon(Icons.add_circle),
                ),
           
              ],
            ),
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
                  Padding(
              padding: const EdgeInsets.only(left: 25, bottom: 10),
              child: IconButton(
                  onPressed: () {
                    controller.removeallProduct(product);
                  },
                  icon: SvgPicture.asset(AssetPath.x)),
            ),
               Padding(
                  padding: const EdgeInsets.only(top:30,left: 55),
                  child: RichText(
                      text: TextSpan(
                          text: r" $"
                              '${products[index].price.toString()}\n',
                          style: TextStyle(
                              fontFamily: 'Gilroy-bold',
                              fontWeight: FontWeight.w600,
                              fontSize: 17,
                              color: Colors.black))),
                ),
          ],
        )
      ],
    );
  }
}
