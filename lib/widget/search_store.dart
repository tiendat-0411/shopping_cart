import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shopping_cart/utils/asset_path.dart';

class Searchstore extends StatelessWidget {
  const Searchstore({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 15),
        child: TextField(
          style: TextStyle(fontSize: 16, color: Colors.black),
          decoration: InputDecoration(
            
            prefixIcon: Padding(
              padding:  EdgeInsets.all(12),
              child: (SvgPicture.asset(AssetPath.search)),
            ),
           
            hintText: 'Search',
            hintStyle: TextStyle(
                color: Color(0xff7C7C7C), fontSize: 14, fontFamily: 'Gilroy'),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
          ),
        ),
      ),
    );
  }
}
