import 'package:flutter/material.dart';

import 'package:shopping_cart/utils/asset_path.dart';

class ACcountSCreen extends StatefulWidget {
  const ACcountSCreen({super.key});

  @override
  State<ACcountSCreen> createState() => _ACcountSCreenState();
}

class _ACcountSCreenState extends State<ACcountSCreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 60, left: 15, right: 15),
      child: Scaffold(
        body: Column(
             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(
                    backgroundColor: Colors.black,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Nguyen Tien Dat',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    Text(
                      'ngtiendat0411@gmail.com',
                      style: TextStyle(color: Colors.grey, fontSize: 16),
                    )
                  ],
                )
              ],
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Row(
              children: [
                Icon(Icons.shopping_bag_sharp),
                Text(
                  'Order',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                IconButton(onPressed: () {}, icon: Icon(Icons.navigate_next))
              ],
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Row(
              children: [
                Icon(Icons.info_rounded),
                Text(
                  'My Details',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                IconButton(onPressed: () {}, icon: Icon(Icons.navigate_next))
              ],
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Row(
              children: [
                Icon(Icons.location_on),
                Text(
                  'Delivery Address',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                IconButton(onPressed: () {}, icon: Icon(Icons.navigate_next))
              ],
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Row(
              children: [
                Icon(Icons.payment_outlined),
                Text(
                  'Payment Methods ',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                IconButton(onPressed: () {}, icon: Icon(Icons.navigate_next))
              ],
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Row(
              children: [
                Icon(Icons.card_travel),
                Text(
                  'Promo Card',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                IconButton(onPressed: () {}, icon: Icon(Icons.navigate_next))
              ],
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Row(
              children: [
                Icon(Icons.notification_important),
                Text(
                  'Noties',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                IconButton(onPressed: () {}, icon: Icon(Icons.navigate_next))
              ],
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Row(
              children: [
                Icon(Icons.help_outlined),
                Text(
                  'Help',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                IconButton(onPressed: () {}, icon: Icon(Icons.navigate_next))
              ],
            ),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            Row(
              children: [
                Icon(Icons.more_vert),
                Text(
                  'About',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
                IconButton(onPressed: () {}, icon: Icon(Icons.navigate_next))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
