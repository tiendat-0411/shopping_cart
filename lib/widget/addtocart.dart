import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:shopping_cart/screen/Cart.dart';

class AddtoCart extends StatelessWidget {
  const AddtoCart({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 400,
        height: 60,
        child: ElevatedButton(
            onPressed: (() {
              Get.offAll(CartScreen());
            }),
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Color(0xff53B175)),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15))))),
            child: Text(
              'Add all to cart',
              style: TextStyle(
                  fontFamily: 'Gilroy',
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                  color: Colors.white),
            )),
      ),
    );
  }
}
